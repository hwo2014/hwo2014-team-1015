using System;
using Newtonsoft.Json;
using System.Collections.Generic;

class MsgWrapper
{
	public string msgType;
	public object data;

	public MsgWrapper(string msgType, object data)
	{
		this.msgType = msgType;
		this.data = data;
	}
}
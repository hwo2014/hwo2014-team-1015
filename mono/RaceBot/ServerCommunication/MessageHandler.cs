﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;

public class MessageHandler
{
	public event System.Action<YourCarMsg> OnMsgReceivedYourCar;
	public event System.Action<GameInitMsg> OnMsgReceivedGameInit;
    public event System.Action<GameStartMsg> OnMsgReceivedGameStart;
	public event System.Action<CarPositionsMsg> OnMsgReceivedCarPositions;
    public event System.Action<TurboMsg> OnMsgReceivedTurboAvailable;

	public string msgType;
	
	public void ParseJson(string message)
	{
		var definition = new { msgType = "" };
		var msg = JsonConvert.DeserializeAnonymousType(message, definition);
		msgType = msg.msgType;

		switch (msgType)
		{
		    case "yourCar":
			    RaiseOnMsgReceivedYourCar(message);
			    break;
		    case "gameInit":
			    RaiseOnMsgReceivedGameInit(message);
			    break;
            case "gameStart":
                RaiseOnMsgReceivedGameStart(message);
                break;
		    case "carPositions":
			    RaiseOnMsgReceivedCarPositions(message);
			    break;
            case "turboAvailable":
                RaiseOnMsgReceivedTurboAvailable(message);
                break;
            //TODO: handle missing messages ("dnf", "gameStart", "gameEnd", "tournamentEnd", "crash", "spawn", "finish"
            //TODO: other useful messages to handle: "createRace", "joinRace"

                // Fallback for when message not recognized (to void client time out in the sever's "eyes")
            default:
                BotApp.Instance.Send(new SendPingMsg());
                break;
		}
	}

	public void RaiseOnMsgReceivedYourCar(string message)
	{
		if (OnMsgReceivedYourCar != null)
		{
			YourCarMsg yourCarMsg = JsonConvert.DeserializeObject<YourCarMsg> (message);
			OnMsgReceivedYourCar (yourCarMsg);
		}
	}

	public void RaiseOnMsgReceivedGameInit(string message)
	{
		if (OnMsgReceivedGameInit != null)
		{
			GameInitMsg gameInitMsg = JsonConvert.DeserializeObject<GameInitMsg> (message);
			OnMsgReceivedGameInit (gameInitMsg);
		}
	}

    public void RaiseOnMsgReceivedGameStart(string message)
    {
        if (OnMsgReceivedGameStart != null)
        {
            GameStartMsg gameStartMsg = JsonConvert.DeserializeObject<GameStartMsg>(message);
            OnMsgReceivedGameStart(gameStartMsg);
        }
    }

	public void RaiseOnMsgReceivedCarPositions(string message)
	{
		if (OnMsgReceivedCarPositions != null)
		{
			CarPositionsMsg carPositionsMsg = JsonConvert.DeserializeObject<CarPositionsMsg>(message);
			OnMsgReceivedCarPositions(carPositionsMsg);
		}
	}

    public void RaiseOnMsgReceivedTurboAvailable(string message)
    {
        if (OnMsgReceivedTurboAvailable != null)
        {
            TurboMsg turboMsg = JsonConvert.DeserializeObject<TurboMsg>(message);
            OnMsgReceivedTurboAvailable(turboMsg);
        }
    }
}

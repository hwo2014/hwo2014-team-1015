﻿using System.Collections.Generic;

public class GameEndMsg : Msg
{
	public Data data { get; set; }

	public class Data
	{
		public List<Results> results { get; set; }
		public List<BestLap> bestLaps { get; set; }
	}
		
	public class Result
	{
		public int? laps { get; set; }
		public int? ticks { get; set; }
		public int? millis { get; set; }
	}

	public class BestLap
	{
		public Car car { get; set; }
		public Result result { get; set; }
	}

	public class Results
	{
		public Car car { get; set; }
		public Result result { get; set; }
	}

	public class Car
	{
		public string name { get; set; }
		public string color { get; set; }
	}
}
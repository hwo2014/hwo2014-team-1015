﻿public class YourCarMsg : Msg
{
	public class Data
	{
		public string name { get; set; }
		public string color { get; set; }
	}

	public Data data { get; set; }
}
﻿using System.Collections;

public class GameStartMsg : Msg 
{
    public int gameTick { get; set; }
}

﻿using System;

public class CarCrashMsg : Msg
{
	public Data data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }

	public class Data
	{
		public string name { get; set; }
		public string color { get; set; }
	}
}

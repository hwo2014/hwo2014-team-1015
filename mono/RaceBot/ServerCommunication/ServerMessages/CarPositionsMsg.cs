﻿using System.Collections.Generic;

public class CarPositionsMsg : Msg
{
	public List<Data> data { get; set; }
	public string gameId { get; set; }
	public int gameTick { get; set; }

	public class Lane
	{
		public int startLaneIndex { get; set; }
		public int endLaneIndex { get; set; }
	}

	public class PiecePosition
	{
		public int pieceIndex { get; set; }
		public double inPieceDistance { get; set; }
		public Lane lane { get; set; }
		public int lap { get; set; }
	}

	public class Data
	{
		public Id id { get; set; }
		public double angle { get; set; }
		public PiecePosition piecePosition { get; set; }
	}
}

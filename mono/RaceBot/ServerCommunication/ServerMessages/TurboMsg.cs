﻿using System.Collections;

public class TurboMsg : Msg 
{
    public Data data { get; set; }

    public class Data
    {
        public double turboDurationMilliseconds { get; set; }
        public int turboDurationTicks { get; set; }
        public double turboFactor { get; set; }
    }    
}

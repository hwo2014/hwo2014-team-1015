using System;
using Newtonsoft.Json;

public abstract class SendMsg
{
	public string ToJson()
	{
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData()
	{
		return this;
	}

	protected abstract string MsgType();
}

public class SendJoinMsg: SendMsg
{
	public string name;
	public string key;
//	public string color;

	public SendJoinMsg(string name, string key) 
    {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() 
    {
		return "join";
	}

    public override string ToString()
    {
        return "name = " + name + " key = " + key;
    }
}

public class SendCustomJoinMsg : SendMsg
{
    public class BotId
    {
        public string name;
        public string key;
    }

    public BotId botId { get; set; }
    public string trackName;
    public string password;
    public int carCount;

    public SendCustomJoinMsg(string botName, string botKey, string trackName, string password, int carCount)
    {
        botId = new BotId();
        botId.name = botName;
        botId.key = botKey;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }

    public override string ToString()
    {
        return "name = " + botId.name +
               " key = " + botId.key +
               " trackName = " + trackName +
               " password = " + password +
               " carCount = " + carCount;
    }
}

public class SendPingMsg: SendMsg 
{
	protected override string MsgType() 
    {
		return "ping";
	}
}

public class SendThrottleMsg: SendMsg 
{
	public double value;

    public SendThrottleMsg(double value)
    {
		this.value = value;
	}

	protected override Object MsgData() 
    {
		return value;
	}

	protected override string MsgType() 
    {
		return "throttle";
	}
}

/// <summary>
/// Only a single switch per car can occur on the same track piece at a time, regardless of the number of lanes on the track.
/// </summary>
public class SendSwitchLaneMsg: SendMsg
{
    public string value;

    public SendSwitchLaneMsg(string switchDirection)
    {
        this.value = switchDirection;
	}

	protected override Object MsgData() 
    {
		return value;
	}

	protected override string MsgType() 
    {
		return "switchLane";
	}
}

public class SendTurboMsg: SendMsg
{
    public string value;

    public SendTurboMsg(string turboText)
    {
        value = turboText;
    }

    protected override object MsgData()
    {
        return value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}
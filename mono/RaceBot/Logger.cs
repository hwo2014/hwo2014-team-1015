﻿//#define DEBUG_ON

using System;

public class Logger
{
	public static void Log(string msg)
	{
		#if DEBUG_ON
		Console.WriteLine(msg);
		#endif
	}

	public static void Break()
	{
		#if DEBUG_ON
	//	Console.ReadLine();
		#endif
	}
}


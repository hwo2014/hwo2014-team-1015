﻿using System;
using System.Collections.Generic;

//TrackInfo details should be processed here
public class TrackInfo
{
    public List<GameInitMsg.Piece> pieces;
    public List<GameInitMsg.Lane> lanes;
    public GameInitMsg.StartingPoint startingPoint;

	public TrackInfo(GameInitMsg.Track trackInfo)
	{
        pieces = trackInfo.pieces;
        lanes = trackInfo.lanes;
        startingPoint = trackInfo.startingPoint;
	}
}

﻿using System;

public class Car
{
    #region Car id
    public string name;
    public string colorId;
    #endregion Car id

    #region Car dimensions
    public double length;
    public double width;
    public double pivotOffset;
    #endregion Car dimensions

    // This data will be updated every frame
    #region Car update info
    public int lap;
    public double angle;
    public int pieceIdx;
    public double pieceDistance;
    public int startLaneIdx;
    public int endLaneIdx;

    // Is updated by the Race framework when the Turbo message is received (only updated for the AICar)
    public bool hasTurbo;
    public TurboMsg.Data turboInfo;
    #endregion Car update info

	protected Race race;

	public int lastTick = 0;
	public int lastPieceIdx = 0;
	public double lastDist = 0;
	public double speed = 0;

    // Total car distance from the starting of the race
    public double totalDist = 0.0;

	public Car (Race _race, GameInitMsg.Car carInfo)
	{
		race = _race;

        name = carInfo.id.name;
        colorId = carInfo.id.color;

        length = carInfo.dimensions.length;
        width = carInfo.dimensions.width;
        pivotOffset = carInfo.dimensions.guideFlagPosition;

        Logger.Log(string.Format("Initialized car: name={0}, color={1}, length={2}, width={3}, pivotOffset={4}", name, colorId, length, width, pivotOffset));
	}

    public void UpdateInfo(CarPositionsMsg.Data carInfo, int gameTick)
    {

		if (gameTick != lastTick)
		{
            int currentPieceIdx = pieceIdx;
            double currentDist = pieceDistance;

            if (carInfo != null)
            {
                currentPieceIdx = carInfo.piecePosition.pieceIndex;
                currentDist = carInfo.piecePosition.inPieceDistance;
            }

			double deltaDist = 0;
			if (currentPieceIdx == pieceIdx)
			{
				deltaDist = currentDist - lastDist;
			}
			else if (race.trackInfo.pieces[pieceIdx].angle.HasValue)
			{
				deltaDist = Math.Abs(race.trackInfo.pieces[pieceIdx].angle.Value) * (race.trackInfo.pieces[pieceIdx].radius.Value - 
				            race.trackInfo.lanes[endLaneIdx].distanceFromCenter * Math.Sign(race.trackInfo.pieces[pieceIdx].angle.Value)) * Math.PI / 180.0 - lastDist + currentDist;
			}
			else
			{
				deltaDist = race.trackInfo.pieces[pieceIdx].length - lastDist + currentDist;
			}

            totalDist += deltaDist;
			speed = deltaDist / (gameTick - lastTick);

			lastDist = currentDist;
			lastTick = gameTick;
		}

        if (carInfo != null)
        {
            lap = carInfo.piecePosition.lap;
            angle = carInfo.angle;
            pieceIdx = carInfo.piecePosition.pieceIndex;
            pieceDistance = carInfo.piecePosition.inPieceDistance;
            startLaneIdx = carInfo.piecePosition.lane.startLaneIndex;
            endLaneIdx = carInfo.piecePosition.lane.endLaneIndex;
        }
    }

    public bool IsLaneSwitching()
    {
        return startLaneIdx != endLaneIdx;
    }

    public bool CanSwitchLane()
    {
        if ( !IsLaneSwitching() )
        {
			int nextPieceIdx = (pieceIdx + 1) % race.trackInfo.pieces.Count;
			return race.trackInfo.pieces[nextPieceIdx].@switch.HasValue && race.trackInfo.pieces[nextPieceIdx].@switch.Value;
        }
        
        return false;
    }

    public virtual void OnTurboAvailable() { }
}


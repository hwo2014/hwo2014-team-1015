﻿using System;
using System.Collections.Generic;

public class Race
{
	public TrackInfo trackInfo;
	public AICar ourCar;
	public List<Car> opponents;
    public List<Car> carsList;

    /// <summary>
    /// All cars in the race corresponding to their unique color id for faster access when receiving from new info from server.
    /// <key> car color id; <value> reference to the race Car 
    /// </summary>
    public Dictionary<string, Car> carsDict;

    private YourCarMsg.Data ourCarData;
    public bool carPositionsUpdated = false;

    // Current race game tick updated when receiving CarPositions message.
    public int gameTick;

	public Race (MessageHandler msgHandler)
	{
        carsList = new List<Car>();
        opponents = new List<Car>();
        carsDict = new Dictionary<string, Car>();

        // Register to server messages.
		msgHandler.OnMsgReceivedYourCar += HandleOnMsgReceivedYourCar;
		msgHandler.OnMsgReceivedGameInit += HandleOnMsgReceivedGameInit;
        msgHandler.OnMsgReceivedGameStart += HandleOnMsgReceivedGameStart;
		msgHandler.OnMsgReceivedCarPositions += HandleOnMsgReceivedCarPositions;
        msgHandler.OnMsgReceivedTurboAvailable += HandleOnMsgReceivedTurboMsg;
	}

    #region Messages handling
    void HandleOnMsgReceivedYourCar(YourCarMsg _yourCarMsg)
    {
        // Get our car id
        ourCarData = _yourCarMsg.data;

        BotApp.Instance.Send(new SendPingMsg());
    }

    void HandleOnMsgReceivedGameInit(GameInitMsg gameInitMsg)
    {
        // Initialize race data structures.
        Logger.Log("[Race] Received GameInit!");

        // Init cars
        List<GameInitMsg.Car> cars = gameInitMsg.data.race.cars;
        foreach(var carInfo in cars)
        {
            if (carInfo.id.color == ourCarData.color)
            {
                Logger.Log("[Race] Found our car with color: " + carInfo.id.color);
                // We found our car
                ourCar = new AICar(this, carInfo);
                carsDict[ourCar.colorId] = ourCar;
                carsList.Add(ourCar);
            }
            else
            {
                Logger.Log("[Race] Found opponent car: " + carInfo.id.color);
                Car newOpponent = new Car(this, carInfo);
                opponents.Add(newOpponent);
                carsList.Add(newOpponent);
                carsDict[newOpponent.colorId] = newOpponent;
            }
        }

        // Init track info.
        trackInfo = new TrackInfo(gameInitMsg.data.race.track);

        BotApp.Instance.Send(new SendPingMsg());
    }

    void HandleOnMsgReceivedGameStart(GameStartMsg gameStartMsg)
    {
        Logger.Log("[Race] Received GameStart: " + gameStartMsg.gameTick);
        BotApp.Instance.Send(new SendPingMsg());
    }

    void HandleOnMsgReceivedCarPositions(CarPositionsMsg carsPosMsg)
    {
        // Safety flag to make sure that we at least once got an update of the car positions.
        //Note: noticed that the protocol behaved different in some runs where initial carPositions msg wasn't sent causing issues.

        if (!carPositionsUpdated)
        {
            Logger.Log("[Race] Received CarPositions: " + carsPosMsg.gameTick);
        }
        carPositionsUpdated = true;

        // Update race cars info (opponents and our car)
        List<CarPositionsMsg.Data> carsInfo = carsPosMsg.data;
        foreach(var carInfo in carsInfo)
        {
            carsDict[carInfo.id.color].UpdateInfo(carInfo, carsPosMsg.gameTick);
        }
        
        gameTick = carsPosMsg.gameTick;
        ourCar.Update();
    }

    void HandleOnMsgReceivedTurboMsg(TurboMsg turboMsg)
    {
        ourCar.hasTurbo = true;
        ourCar.turboInfo = turboMsg.data;

        ourCar.OnTurboAvailable();
    }

    #endregion Messages handling

    #region Utility methods
    /// <summary>
    /// Gets a close enough car to the target car, on the same lane.
    /// </summary>
    /// <param name="targetCar"></param>
    /// <returns></returns>
    public Car GetClosestCarTo(Car targetCar, bool inFrontOfTargetCar = true)
    {
		for (int i = 0; i < carsList.Count; ++i)
		{
			if (carsList[i].endLaneIdx != targetCar.endLaneIdx)
			{
				continue;
			}

			if ((carsList[i].pieceIdx > targetCar.pieceIdx && carsList[i].pieceIdx <= targetCar.pieceIdx + 2) ||
			    (carsList[i].pieceIdx + trackInfo.pieces.Count > targetCar.pieceIdx && carsList[i].pieceIdx + trackInfo.pieces.Count <= targetCar.pieceIdx + 2) ||
			    (carsList[i].pieceIdx == targetCar.pieceIdx && carsList[i].pieceDistance > targetCar.pieceDistance))
			{
				return carsList[i];
			}
		}
		
		return null; //no matching car
    }
    #endregion Utility methods
}

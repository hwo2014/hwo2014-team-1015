﻿using System.Collections;
using System;

public class AICar : Car 
{
    public enum LaneSwitchDir : int
    {
        None = 0,
        Left = -1,
        Right = 1,
    }

    //public const double switchLaneAngleLimit = 10;
    // If front car is within this distance, plan lane switch ahead of time
    public const double frontCarSwitchDistance = 250;

    #region Car outputs
    
    public double throttle = 0;
    public double lastAppliedThrottle = 0;

    // Currently targeted laneSwitchDirection.
    public LaneSwitchDir curLaneSwitchDir = LaneSwitchDir.None;
    // Indicates if a message has already been sent this tick (other messages shouldn't try to send until the next tick).
    protected bool msgSentThisTick = false;
    
    #endregion

    protected int lastLaneSwitchGameTick = -1;
	protected int lastLaneSwitchPieceIdx = -10;
    protected int lastCarAvoidTargetLane = -1;

    public double curPieceAngle;
    protected double lastAngle = 0;

    int nextPieceIdx;
    double nextPieceAngle;
    double maxNextPieceAngle;

    int nextAngledPieceIdx;
    double nextAngledPieceAngle;
    double distToNextAngledPiece;
    int piecesToNextAngledPiece;

	double nextAngledPieceSwitchAngle;


    public AICar(Race _race, GameInitMsg.Car carInfo) : base(_race, carInfo)
    {
        Logger.Log("[AICar] Initialized car " + carInfo.id.name + " -> " + carInfo.id.color);
    }

    #region Car server commands
    public void ApplyThrottle(double newThrotle)
    {
        if (!msgSentThisTick)
        {
            BotApp.Instance.Send(new SendThrottleMsg(newThrotle));
            lastAppliedThrottle = newThrotle;
            msgSentThisTick = true;
        }
    }

    public void SwitchLaneTo(LaneSwitchDir laneDir)
    {
        if (!msgSentThisTick && laneDir != LaneSwitchDir.None)
        {
            lastLaneSwitchGameTick = race.gameTick;
			lastLaneSwitchPieceIdx = nextPieceIdx;

			Logger.Log("Switching lane message to " + laneDir);
            BotApp.Instance.Send( new SendSwitchLaneMsg(System.Enum.GetName(typeof(LaneSwitchDir), laneDir)) );
            msgSentThisTick = true;
        }
    }

    public void UseTurbo()
    {
        if (hasTurbo)
        {
            BotApp.Instance.Send(new SendTurboMsg("iiiiihaaaaaa!!!"));
            msgSentThisTick = true;
            hasTurbo = false;
        }
        else
        {
            Logger.Log("WARNING! Trying to use turbo WITHOUT having it!");
        }
    }
    #endregion Car server commands

    double GetMaxSpeed(double pieceAngle)
    {
        return 2.5 + Math.Max((90.0 - Math.Abs(pieceAngle)) / 90.0 * 8.0, 0.0);
    }

    /// <summary>
    /// Called by the Race framework when the turbo message is received.
    /// "turboInfo" structure will be already updated when this method is called by the race framework.
    /// </summary>
    public override void OnTurboAvailable()
    {
//        Logger.Log("Received TURBO!!!");
        TryToUseTurbo();
    }

    /// <summary>
    /// If there's turbo available try to use it.
    /// Decide here if we can/should use turbo.
    /// This method is called in the Update() method.
    /// </summary>
    public void TryToUseTurbo()
    {
        // Check if we have turbo available and no other message has already been sent this last tick
        if (hasTurbo && !msgSentThisTick)
        {
            //Note: call this method to use turbo when conditions are met.
            // UseTurbo();
        }
    }

    /// <summary>
    /// Decide here if we can/should switch lanes and apply switch.
    /// </summary>
    public void TryToSwitchLane()
    {
        // The currently decided target lane switch direction
        curLaneSwitchDir = LaneSwitchDir.None;

        if (!msgSentThisTick && CanSwitchLane() )
        {
            // Don't allow consecutive lane switching so we have a breather for throttle control too.
            if (nextPieceIdx != lastLaneSwitchPieceIdx && race.gameTick - lastLaneSwitchGameTick >= 2)
            {
                // Check if we need to do a right switch unless we're already on the maximum right lane.
				if (nextAngledPieceSwitchAngle > 0.0 && startLaneIdx < race.trackInfo.lanes.Count - 1)
                {
                    curLaneSwitchDir = LaneSwitchDir.Right;
                    lastCarAvoidTargetLane = -1;
                    /*Logger.Log("switch lane Right because nextPieceAngle = " + nextPieceAngle + 

                        " startLaneIdx = " + startLaneIdx +
                        " endLaneIdx = " + endLaneIdx +
                        " -> gameTick = " + race.gameTick);*/
                }
				else if (nextAngledPieceSwitchAngle < 0.0 && startLaneIdx > 0)
                {
                    curLaneSwitchDir = LaneSwitchDir.Left;
                    lastCarAvoidTargetLane = -1;
                    /*Logger.Log("switch lane Left because nextPieceAngle = " + nextPieceAngle +
                        " startLaneIdx = " + startLaneIdx +
                        " endLaneIdx = " + endLaneIdx +
                        " -> gameTick = " + race.gameTick);*/
                }
            }

            // Try to override above lane switch decision if we need to avoid a car in front of us.
            Car otherFrontCar = race.GetClosestCarTo(this);
            if (otherFrontCar != null)
            {
				Logger.Log(otherFrontCar.name + " in front of me");

				if (nextAngledPieceSwitchAngle > 0.0)
				{
					curLaneSwitchDir = LaneSwitchDir.Right;
				}
				else if (nextAngledPieceSwitchAngle < 0.0)
				{
					curLaneSwitchDir = LaneSwitchDir.Left;
				}
				
				int newLaneIdx = endLaneIdx;
				if ((int)curLaneSwitchDir > 0)
				{
					newLaneIdx = (newLaneIdx + (int)curLaneSwitchDir) % race.trackInfo.lanes.Count;
				}
	            else if ((int)curLaneSwitchDir < 0)
	            {
	                newLaneIdx--;
	                if (newLaneIdx < 0)
	                    newLaneIdx = race.trackInfo.lanes.Count - 1;
	            }

	            // Calculate lane signed offset to determine the new switch direction for car avoidance.
	            LaneSwitchDir newLaneSwitchDir = (LaneSwitchDir)Math.Sign(newLaneIdx - endLaneIdx);

	            // Calculate new target lane idx
	            int newTargetLaneIdx = endLaneIdx + (int)newLaneSwitchDir;
	            if (newTargetLaneIdx >= race.trackInfo.lanes.Count || newTargetLaneIdx < 0)
				{
	                newTargetLaneIdx = endLaneIdx;
				}

				Logger.Log("end lane: " + endLaneIdx + "  new lane: " + newTargetLaneIdx + " last target lane: " + lastCarAvoidTargetLane);

	            if (newTargetLaneIdx != endLaneIdx && lastCarAvoidTargetLane != newTargetLaneIdx)
	            {
	                // Override the current lane switch direction.
	                curLaneSwitchDir = newLaneSwitchDir;
	                lastCarAvoidTargetLane = newTargetLaneIdx;
	                Logger.Log("SwitchLane avoidance for car " + otherFrontCar.colorId +
	                            " newSwitchDir = " + curLaneSwitchDir +
	                            " targetLaneIdx = " + newTargetLaneIdx +
	                            " gameTick = " + race.gameTick);
	            }
				else 
				{
					curLaneSwitchDir = LaneSwitchDir.None;
				}
            }

            // At the end apply the decided lane switch direction
            SwitchLaneTo(curLaneSwitchDir);
        }
    }
	// An angle that also takes into account the steepness of the curve based on its radius. The bigger the radius, the less steep it is, so the result is smaller
	protected double GetPieceScaledAngle(int pieceIndex)
	{
		if (!race.trackInfo.pieces[pieceIndex].angle.HasValue)
		{
			return 0.0;
		}

		//Logger.Log("Angle: " + race.trackInfo.pieces[pieceIndex].angle.Value + "Radius: " + race.trackInfo.pieces[pieceIndex].radius.Value);
		double pieceAngle = race.trackInfo.pieces[pieceIndex].angle.Value;
		double pieceRadius = race.trackInfo.pieces[pieceIndex].radius.Value;
		double fiftyFactor = 0.65;
		double radiusFactor = (pieceRadius - 100.0) * (1.0 - fiftyFactor) / 50.0 + 1.0;
		return pieceAngle * 1.0 / radiusFactor;
	}

    public void Update()
    {
		curPieceAngle = GetPieceScaledAngle(pieceIdx);//race.trackInfo.pieces[pieceIdx].angle.HasValue ? race.trackInfo.pieces[pieceIdx].angle.Value : 0.0;
        nextPieceIdx = (pieceIdx + 1) % race.trackInfo.pieces.Count;
		nextPieceAngle = GetPieceScaledAngle(nextPieceIdx);//race.trackInfo.pieces[nextPieceIdx].angle.HasValue ? race.trackInfo.pieces[nextPieceIdx].angle.Value : 0.0;
		nextAngledPieceIdx = pieceIdx;
		distToNextAngledPiece = curPieceAngle == 0.0 ? race.trackInfo.pieces[pieceIdx].length - pieceDistance : 0.0;
		piecesToNextAngledPiece = 0;

		bool doneNextAngle = false;
		bool doneNextSwitchAngle = false;

		for (int i = nextPieceIdx; i != pieceIdx; i = (i + 1) % race.trackInfo.pieces.Count)
		{
			if (race.trackInfo.pieces[i].angle.HasValue)
			{
				if (!doneNextAngle) 
				{
					nextAngledPieceIdx = i;
	                nextAngledPieceAngle = race.trackInfo.pieces[i].angle.Value;
					doneNextAngle = true;
				}

				if (i != nextPieceIdx)
				{
					nextAngledPieceSwitchAngle = race.trackInfo.pieces[i].angle.Value;
					doneNextSwitchAngle = true;
				}
			}
			else if (!doneNextAngle)
			{
				piecesToNextAngledPiece++;
				distToNextAngledPiece += race.trackInfo.pieces[i].length;
			}

			if (doneNextAngle && doneNextSwitchAngle)
			{
				break;
			}
		}

		maxNextPieceAngle = Math.Max(Math.Abs(nextPieceAngle), Math.Abs(curPieceAngle));//Math.Abs(Math.Abs(nextPieceAngle) - Math.Abs(curPieceAngle));
//		nextAngledPieceAngle = Math.Abs(race.trackInfo.pieces[nextAngledPieceIdx].angle.Value);

		double pathAngleThrottle = 0.1 + Math.Max ((90.0 - maxNextPieceAngle) / 90.0 * 0.9, 0.0);
		double carAngleThrottle = (Math.Abs(angle) - Math.Abs(lastAngle)) > 0.0 ? (0.0 + Math.Max((20.0 - Math.Abs(angle)) / 20.0 * 1.0, 0.0)) : 1.0;
		lastAngle = angle;

		double maxSpeed = 50.0;
		if (maxNextPieceAngle >= 10)
		{
			maxSpeed = GetMaxSpeed(maxNextPieceAngle);
		}

		if (distToNextAngledPiece < 300)
		{
			distToNextAngledPiece *= 0.4; //to make the max speed tend faster towards the new value;
//			Logger.Log("Starting to modify speed: " + maxSpeed + " at dist: " + distToNextAngledPiece);
			maxSpeed = Math.Min(maxSpeed, (Math.Min(maxSpeed, 8.0) * distToNextAngledPiece + GetMaxSpeed(GetPieceScaledAngle(nextAngledPieceIdx)) * (300.0 - distToNextAngledPiece)) / 300.0);
//			Logger.Log("Modified speed: " + maxSpeed + " speed: " + speed);
		}
		
		throttle = Math.Min(pathAngleThrottle, carAngleThrottle) * maxSpeed / speed; //it will make it always tend to drive with maxSpeed
		if (maxSpeed < speed)
		{
			throttle *= maxSpeed / speed; //break harder
		}

        throttle = Math.Min(Math.Max(throttle, 0), 1);

		//Logger.Log("[AICar] angle current: " + curPieceAngle + "; angle next: " + nextPieceAngle + " => new throttle: " + throttle + " | pathThrottle: " + pathAngleThrottle + " carThrottle: " + carAngleThrottle);

		//Logger.Log("[AICar] speed: " + speed + "; angle: " + angle);

        // Try to switch lanes if we can/should no other messages were already sent this tick.
        TryToSwitchLane();

        // Try to use turbo if available and conditions are met, if no other messages were already sent this tick.
        TryToUseTurbo();

        // Apply throtle if no other message was previously sent to the server (turbo or lane switch)
        ApplyThrottle(throttle);

        // Reset message sent flag.
        msgSentThisTick = false;
    }

}

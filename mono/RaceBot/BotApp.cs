using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;

public class BotApp
{
    private static BotApp instance;

	public static volatile bool useVisualDebugger = false;

    private StreamReader reader;
    private StreamWriter writer;
    private StreamWriter debugWriter;

    public Race race;
	public MessageHandler msgHandler;
    
    public volatile bool stopBot = false;


	public static void Main(string[] args) 
	{
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];


        new BotApp().Connect(host, port, new SendJoinMsg(botName, botKey));
	}

	public BotApp()
	{
		instance = this;

		msgHandler = new MessageHandler();
		race = new Race(msgHandler);

//			switch(msg.msgType)
//			{
//				case "carPositions":
//					send (new Throttle (0.657));
//					break;
//				case "join":
//					Console.WriteLine("Joined");
//					send(new Ping());
//					break;
//				case "gameInit":
//					Console.WriteLine("Race init");
//					send(new Ping());
//					break;
//				case "gameEnd":
//					Console.WriteLine("Race ended");
//					send(new Ping());
//					break;
//				case "gameStart":
//					Console.WriteLine("Race starts");
//					send(new Ping());
//					break;
//				default:
//					send(new Ping());
//					break;
//			}
	}

    public static BotApp Instance
    {
        get
        {
            return instance;
        }
    }

    public void Connect(string host, int port, SendMsg joinMsg, bool connectToVisualizerServer = false, int visualizerServerPort = 8092)
    {
        string line;
        TcpClient client = null;
        TcpClient debugVisualizerClient = null;

        useVisualDebugger = connectToVisualizerServer;
        Console.WriteLine("Bot connecting to " + host + ":" + port + " as " + joinMsg.ToString());

        try
        {
            client = new TcpClient(host, port);
            NetworkStream stream = client.GetStream();
            reader = new StreamReader(stream);
            writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            if (useVisualDebugger)
            {
                try
                {
                    debugVisualizerClient = new TcpClient("127.0.0.1", visualizerServerPort);
                    NetworkStream debugStream = debugVisualizerClient.GetStream();
                    debugWriter = new StreamWriter(debugStream);
                    debugWriter.AutoFlush = true;
                }
                catch (System.Exception ex)
                {
                    Logger.Log("[BotApp] Connect: failed to connect to visualizer server: " + ex.Message);
                    useVisualDebugger = connectToVisualizerServer = false;
                }
            }

            Send(joinMsg);

            while ( !stopBot )
            {
                if (reader.Peek() < 0)
                {
                    System.Threading.Thread.Sleep(5);
                    continue;
                }

                line = reader.ReadLine();
                msgHandler.ParseJson(line);

                if (useVisualDebugger)
                {
                    // Send the received line from the AI bot server to the local visual debugger server.
                    debugWriter.WriteLine(line);
                }
            }
        }
        catch(System.Exception ex)
        {
            Logger.Log("[BotApp] Exception: " + ex.Message + "\nStack: " + ex.StackTrace);
        }
        finally
        {
            useVisualDebugger = false;
            try
            {
                if (client != null)
                {
                    client.Close();
                    client = null;
                }

                if (debugVisualizerClient != null)
                {
                    debugVisualizerClient.Close();
                    debugVisualizerClient = null;
                }
            }
            catch(System.Exception ex)
            {
                Logger.Log("[BotApp] Exception while trying to shutdown race bot: " + ex.Message);
            }
        }
    }

	public void Send(SendMsg msg)
	{
		writer.WriteLine(msg.ToJson());
	}
}
